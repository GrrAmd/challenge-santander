# Informacion del challenge

## Descripción

Challenge de backend realizado para Santander Tecnologia


## Requerimientos (Si se va a ejectur en docker)

* Docker 18.X
* Docker-compose 1.17.X
* Conectividad entre el servidor con Docker y la base de datos.
* Tener la base de datos para la aplicación ya creada.


## Requerimientos (Si se va a ejectur localmente)

* Tener instalado npm
* Tener una base de datos mysql para la aplicacion ya creada


## Instalación con docker
Para instalar con docker, ir a la carpeta Dokcerfiles y ver su documentacion

## Instalación con npm

Procederemos a explicar como desplegar los contenedores en el entorno de desarrollo local.

Lo primero será clonar el repositorio con el comando

```
git clone https://gitlab.com/GrrAmd/challenge-santander.git
```
Luego ingresamos al directorio recién creado con el comando:

```
cd challenge-santander

```
## Backend
Luego nos movemos al directorio correspondiente al backend con el comando:

```
cd ./api

```

Antes de comenzar a levantar el entorno debemos crear el archivo .env, el cual contrandrá todas las variables de entorno que necesitamos

Los comandos para crear estos 2 achivos son:
```
cp .env.template .env 
```

Luego ingresamos al archivo .env y configuramos las variables según creamos conveniente.
```
APP_URL= Aca vamos a poner el link que inicia nuestro backend
APP_PORT= Puerto al cual se va a conectar

DB_HOST = Conexiones para la DB
DB_PORT = Conexiones para la DB
DB_USERNAME = Conexiones para la DB
DB_PASSWORD = Conexiones para la DB
DB_DATABASE = Conexiones para la DB

APP_KEY = Definir una clave unica para aplicacion

RAPID_API_KEY = Api key de OpenWeatherMap que provee Rapid Api

SMTP_SERVICE = Servicio de smtp para el envio de mails
SMTP_USER= Usuario del servicio smtp
SMPT_PASS = Contraseña del servicio smtp

PROVIDER_MAIL= Mail de quien va a actuar como nuestro proveedor
```


Finalmente ejecutamos los siguientes comandos:

```
npm i -g nodemon typescript rimraf tsoa concurrently
npm ci
npm run build
npx typeorm schema:sync #Esto nos carga los modelos en la DB
npm run migrate #Esto nos hace un seed a la DB para ver datos
npm run dev #Ejecuta el servidor

```

Si el script finaliza correctamente veremos algo similar a:

```
[1] [nodemon] to restart at any time, enter `rs`
[1] [nodemon] watching path(s): src\**\*
[1] [nodemon] watching extensions: ts,js
[1] [nodemon] starting `tsoa spec`
[0] [nodemon] to restart at any time, enter `rs`
[0] [nodemon] watching path(s): src\**\*
[0] [nodemon] watching extensions: ts,js
[0] [nodemon] starting `set TS_NODE=true && ts-node ./src/index.ts`
[1] [nodemon] clean exit - waiting for changes before restart
[0] ⚡️[server]: Server is running at http://localhost:3001/api

```

## Frontend
La ejecucion del frontend es bastante sencilla, tan solo ejecutamos
```
npm ci
npm install -g @angular/cli
ng serve -o
```
 Si bien en el frontend me faltaron implementaciones, hice las que pude con el tiempo.

