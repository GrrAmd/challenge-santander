# Despliegue del Validador de Facturas sobre Docker

## Descripción

El objetivo de este script es el de desplegar la aplicación de Validador de Facturas sobre Docker.


## Requerimientos

* Docker 18.X
* Docker-compose 1.17.X
* Conectividad entre el servidor con Docker y la base de datos.
* Tener la base de datos para la aplicación ya creada.


## Instalación

Procederemos a explicar como desplegar los contenedores en el entorno de desarrollo local.

Lo primero será clonar el repositorio con el comando

```
git clone https://gitlab.com/GrrAmd/challenge-santander.git
```
Luego ingresamos al directorio recién creado con el comando:

```
cd challenge-santander

```
Luego nos movemos al directorio correspondiente a Docker con el comando:

```
cd Dockerfiles

```

Antes de comenzar a levantar el entorno debemos crear el archivo .env, el cual contrandrá todas las variables de entorno que necesitamos

Los comandos para crear estos 2 achivos son:
```
cp .env.template .env 
```

Luego ingresamos al archivo .env y configuramos las variables según creamos conveniente.

Finalmente ejecutamos el script para levantar el entorno:

```
docker-compose up -d
```

Si el script finaliza correctamente veremos algo similar a:

```
               Name                             Command               State           Ports         
----------------------------------------------------------------------------------------------------
challenge_birras-backend    docker-entrypoint.sh nodem ...   Up      0.0.0.0:3000->3000/tcp
challenge_birras-frontend   docker-entrypoint.sh ng se ...   Up      0.0.0.0:4200->4200/tcp

```
