import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service';
import { ciudades } from 'src/assets/json/ciudades-argentinas';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) {}
  form: FormGroup;

  provincias = ciudades;
  ciudades;

  ngOnInit(): void {
    this.form = this.fb.group({
      Nombre: ['', Validators.required],
      Fecha: ['', Validators.required],
      Duracion: ['', Validators.required],
      Provincia: ['', Validators.required],
      Ciudad: ['', Validators.required],
      Descripcion: ['', Validators.required],
      UrlImage: ['', Validators.required],
    });
  }

  getCiudades(value) {
    const cities = this.provincias
      .find((prov) => prov.nombre == value)
      .ciudades.filter((val) => val.nombre);

    this.ciudades = [
      ...new Map(cities.map((item) => [item.nombre, item])).values(),
    ];
  }

  async sendForm() {
    try {
      const {
        Nombre,
        Fecha,
        Duracion,
        Ciudad,
        Descripcion,
        UrlImage,
      } = this.form.value;
      const payload = {
        nombre: Nombre,
        fecha: Fecha,
        duracion: Duracion,
        lugar: `${Ciudad}, AR`,
        descripcion: Descripcion,
        urlImage: UrlImage,
      };
      await this.apiService.post('meetup/new', payload).toPromise();
      Swal.fire({
        title: 'Meetup creada correctamente',
        icon: 'success',
        timer: 3000,
        showConfirmButton: false,
      }).then((sw) => {
        if (sw.isDismissed) {
          this.router.navigate(['/admin']);
        }
      });
    } catch (error) {
      console.log('No se pudo crear la meetup');
    }
  }
}
