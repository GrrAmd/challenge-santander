import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MeetupService } from 'src/app/core/services/meetup.service';

@Component({
  templateUrl: './meetup.component.html',
  styleUrls: ['./meetup.component.scss']
})
export class MeetupComponent implements OnInit {
  slug: string;
  data: any;
  isLoading: boolean;

  constructor(
    private route: ActivatedRoute,
    private meetupService: MeetupService
    ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.slug = this.route.snapshot.paramMap.get('slug');
    this.data = this.getMeetup()
  }

  async getMeetup(){
    this.meetupService.getMeetup(this.slug).then(val => {
      this.data = val;
      this.isLoading = false;
    });
  }

}
