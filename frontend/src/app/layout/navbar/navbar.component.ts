import { Component, OnChanges, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services/auth.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  menuItems = [
    {
      label: 'Iniciar Sesion',
      icon: 'login',
      link: '/auth/login',
      logged: false,
    },
    {
      label: 'Registrarse',
      icon: 'app_registration',
      link: '/auth/register',
      logged: false,
    },
    {
      label: 'Mis meetups',
      icon: 'groups',
      link: '/mis-meetups',
      logged: true,
      admin: false
    },
    {
      label: 'Panel de admin',
      icon: 'post_add',
      link: '/admin',
      logged: true,
      admin: true
    },
  ];
  public user: {
    name: string;
    photoUrl: string;
    email: string;
  };

  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  isLogged = false;
  isAdmin = false;
  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationEnd) {
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
    this.isLogged = this.authService.isLoggedIn();
    this.isAdmin = this.authService.isAdmin();
  }

  logout() {
    return this.authService.logout();
  }
}
