import { getConnection, getRepository } from 'typeorm';
import { User } from '../models/User';

export class UserRepository {

  static async getUser(id: number): Promise<User> {
    return await User.getRepository().findOneOrFail(id);
  }

  static async getPassword(email: string){
    return await User
    .createQueryBuilder()
    .select("user.password")
    .addSelect("password")
    .where("email = :email", {email: email})
    .getRawOne()
  }

  static async getUserFull(email: string) {
    return await User
    .createQueryBuilder()
    .addSelect('password')
    .where("user.email = :email", {email})
    .getOne()
  }

  static async checkPassword(password: string) {

  }
}
