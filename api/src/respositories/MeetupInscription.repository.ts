import { MeetupInscription } from '../models/MeetupInscription';
import { User } from '../models/User';
import { MeetupRepository } from './Meetup.repository';

export class MeetupInscriptionRepository {
  static async create(user: User, meetupId: number) {
    const meetup = await MeetupRepository.findOne(meetupId);
    const meetupInscription = new MeetupInscription();
    meetupInscription.user = user;
    meetupInscription.meetup = meetup;
    return meetupInscription.save();
  }

  static async countMembers(id: number) {
    const [meet, total] = await MeetupInscription.findAndCount({ where: { meetup: id } });
    return total;
  }
}
