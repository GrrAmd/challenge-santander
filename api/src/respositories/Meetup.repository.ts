import { Request } from "express";
import moment from "moment";
import { MeetupDto, MeetupsDto } from "../dto/Meetup.dto";
import { IMeetupResponse } from "../interfaces/ReponsesExamples.interface";
import { Meetup } from "../models/Meetup";
import { MeetupImages } from "../models/MeetupImages";
import { MeetupInscription } from "../models/MeetupInscription";
import { User } from "../models/User";
import { UserService } from "../services/User.service";
import { crypt } from "../utils/Jwt.util";

export interface IMeetupPayload {
  nombre: string;
  fecha: Date | string;
  duracion: string;
  lugar: string;
  descripcion: string;
  urlImage: string;
}

export class MeetupRepository {
  static async getMeetups(): Promise<IMeetupResponse[]> {
    try {
      return MeetupsDto((await Meetup.find()) as any);
    } catch (error) {
      throw new Error(error);
    }
  }

  static async getMeetupBySlug(slug): Promise<IMeetupResponse> {
    try {
      const meetup = await Meetup.findOne({ where: { slug: slug } });

      return meetup as any;
    } catch (error) {
      throw new Error(error);
    }
  }

  static async createMeetup(
    request: Request,
    body: IMeetupPayload
  ): Promise<IMeetupResponse> {
    const user = await new UserService().getUserJwt(request);
    const meetup = new Meetup();
    const { nombre, fecha, duracion, descripcion, lugar, urlImage } = body;
    meetup.nombre = nombre;
    meetup.fecha = moment(fecha, "YYYY-MM-DD HH:mm:ss").toDate();
    meetup.duracion = duracion;
    meetup.descripcion = descripcion;
    meetup.lugar = lugar;
    meetup.organizador = user;

    const image = new MeetupImages();
    image.imagePath = urlImage;
    image.save();

    meetup.image = image;
    const saved = await Meetup.save(meetup);

    return MeetupDto(saved as any);
  }

  static async findOne(id: number) {
    return Meetup.findOneOrFail(id);
  }

  static async disableMeetup(meetId: number) {
    return Meetup.update({ id: meetId }, { available: false });
  }

  static async availableMeetups(): Promise<Meetup[]> {
    return Meetup.find({ where: { available: true } });
  }

  public async findByToken(token: string): Promise<Meetup> {
    return Meetup.findOne({ where: { token } });
  }

  static async changeToken(meetup) {
    const tokenize = crypt(meetup);
    Meetup.update({ id: meetup.id }, { token: tokenize });
  }

  static async getMeetupsUser(request: Request) {
    const user = await new UserService().getUserJwt(request);
    return await MeetupInscription.find({ where: { user: user.id } });
  }
}
