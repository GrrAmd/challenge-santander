import { IMeetupResponse } from '../interfaces/ReponsesExamples.interface';
import { Meetup } from '../models/Meetup';
import { User } from '../models/User';

export function MeetupDto(meetup: Meetup): IMeetupResponse {
  delete meetup.token;
  meetup.organizador = meetup.organizador.userMeetup as User;
  return meetup as any;
}

export function MeetupsDto(meetup: Meetup[]): IMeetupResponse[] {
  meetup.map(meet => {
    delete meet.token;
    meet.organizador = meet.organizador.userMeetup as User;
  })
  return meetup as any;
}
