import express from 'express';
import { JWT_SECRET } from '../config/constants';
import * as jwt from "jsonwebtoken";

export function expressAuthentication(request: express.Request, securityName: string, scopes?: string[]): Promise<any> {
  if (securityName === "api_key") {
    const token =
      request.body.token ||
      request.query.token ||
      request.headers["x-access-token"] ||
      request.headers["authorization"];

    return new Promise((resolve, reject) => {
      if (!token) {
        reject(new Error("No token provided"));
      }
      jwt.verify(token, JWT_SECRET, function (err: any, decoded: any) {
        if (err) {
          reject(err);
        } else {
          // Check if JWT contains all required scopes
          console.log(scopes);
          for (let scope of scopes) {
            if (!decoded.scopes.includes(scope)) {
              reject(new Error("JWT does not contain required scope."));
            }
          }
          resolve(decoded);
        }
      });
    });
  }
}
