import { NextFunction, Request, Response } from 'express';
import * as jwt from "jsonwebtoken";
import { JWT_SECRET } from '../config/constants';
import { getJwt } from '../utils/Jwt.util';

export async function isAdmin(request: Request, response: Response, next: NextFunction) {
  const token = getJwt(request);
  
  let { usuario } = jwt.verify(token, JWT_SECRET) as any;

  request['usuario'] = usuario;
  
  if (request['usuario'].role !== 'admin') {
    return response.status(401).json({
      success: false,
      message: "No tiene permiso para esta accion"
    })
  }
  next();
}
