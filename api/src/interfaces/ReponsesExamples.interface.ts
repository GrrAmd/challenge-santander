import { MeetupClima } from '../models/MeetupClima';
import { MeetupImages } from '../models/MeetupImages';

export interface IAuthLoginSuccess {
  success: boolean;
  user: IUserInfo;
  token: string;
}

export interface IErrorGeneric {
  statusCode?: number;
  success: boolean;
  message: any[] | string;
}

interface IUserInfo {
  id: number;
  email: string;
  nombre: string;
  apellido: string;
  role: string;
}

export interface UserLoginParams {
  email: string;
  password: string;
}

export interface AuthRegisterParams {
  email: string;
  nombre: string;
  apellido: string;
  password: string;
}

export interface IRegisterSuccess {
  success: boolean;
  message: string;
  user: {
    email: string;
    nombre: string;
    apellido: string;
  },
  token: string;
}

export interface IMeetupResponse {
  id: number;
  nombre: string;
  fecha?: Date | string;
  duracion: string;
  descripcion: string;
  slug: string;
  lugar: string;
  createdAt?: Date;
  updatedAt?: Date;
  available: boolean;
  organizador: {
    email: string;
    nombre: string;
    apellido: string;
  },
  image: { id: number, imagePath: string },
  clima?: { id: number, date: Date | string, rain: boolean, tempAprox: number }
}

export interface IMeetupCreated {
  id: number;
  nombre: string;
  fecha?: Date | string;
  duracion: string;
  descripcion: string;
  slug: string;
  lugar: string;
  createdAt?: Date;
  updatedAt?: Date;
  available: boolean;
  organizador: {
    email: string;
    nombre: string;
    apellido: string;
  }
}

export interface ISuccessGeneric {
  statusCode: number;
  success: boolean;
  message: any;
}
