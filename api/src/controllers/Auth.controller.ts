import { User } from '../models/User';
import { getRepository } from 'typeorm';
import { validationResult } from 'express-validator';
import { generateJwt } from '../utils/Jwt.util';
import { Request as Req } from "express";
import { UserRepository } from '../respositories/User.repository';
import { Body, Example, Post, Response, Route, Tags, Request, Security } from 'tsoa';
import { AuthRegisterParams, IErrorGeneric, IAuthLoginSuccess, IRegisterSuccess, UserLoginParams } from '../interfaces/ReponsesExamples.interface';
import moment from 'moment';

@Route("api/auth/")
@Tags("Authentication")
export class AuthController {

  @Post("login")
  @Response<IAuthLoginSuccess>(200, "Login success", {
    success: true,
    user: {
      id: 1,
      email: 'admin@admin.com',
      nombre: 'Admin',
      apellido: 'Administrador',
      role: 'admin'
    },
    token: '<Bearer Token>'
  })
  @Response<IErrorGeneric>(401, "Login failed", {
    success: true,
    message: "El usuario y/o contraseña no coinciden o no existe"
  })
  @Response<IErrorGeneric>(400, "Login failed", {
    success: false,
    message: [
      {
        password: {
          msg: "La contraseña no puede estar vacia",
          param: "password",
          location: "body"
        },
        email: {
          msg: "El email no puede estar vacio",
          param: "email",
          location: "body"
        }
      }
    ]
  })
  @Response<IErrorGeneric>(500, "Login failed", {
    statusCode: 500,
    success: false,
    message: 'Ocurrio un error, contacte con el Administrador del sistema'
  })
  public async login(@Body() requestBody?: UserLoginParams): Promise<IAuthLoginSuccess | IErrorGeneric> {
    try {
      const errors = validationResult(requestBody);

      let { email, password } = requestBody;
      if (!errors.isEmpty()) {
        return { success: false, message: errors.array() };
      }

      try {
        const user = await User.findOne({ where: { email } });
        const pass = await UserRepository.getPassword(email);

        if (!user) {
          return { statusCode: 401, success: false, message: 'El usuario y/o contraseña no coinciden o no existe' } as IErrorGeneric;
        }
        user.password = pass.password;
        if (!user.validatePassword(password)) {
          return { statusCode: 401, success: false, message: 'El usuario y/o contraseña no coinciden o no existe' } as IErrorGeneric;
        }

        const token = await generateJwt(user);

        return { statusCode: 200, success: true, user: user.userInfo, token: `Bearer ${token}` } as IAuthLoginSuccess;
      } catch (error) {
        console.log(error)
        return { statusCode: 401, success: false, message: 'Inicio de sesion no autorizado' } as IErrorGeneric;
      }
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Post("register")
  @Response<IRegisterSuccess>(200, "Register success", {
    success: true,
    message: "Usuario registrado",
    user: {
      email: "string@string.com",
      nombre: "string",
      apellido: "string"
    },
    token: "<Bearer Token>"
  })
  @Response<IErrorGeneric>(400, "Register failed", {
    success: true,
    message: [
      {
        password: {
          msg: "La contraseña no puede estar vacia",
          param: "password",
          location: "body"
        },
        email: {
          msg: "El email no puede estar vacio",
          param: "email",
          location: "body"
        }
      }
    ],
  })
  @Response<IErrorGeneric>(500, "Register failed", {
    success: true,
    message: "Ocurrio un error, contacte con el Administrador del sistema",
  })
  public async register(@Body() requestBody: AuthRegisterParams): Promise<IRegisterSuccess | IErrorGeneric> {
    try {
      const errors = validationResult(requestBody);
      if (!errors.isEmpty()) {
        return { statusCode: 400, success: false, message: errors.array() };
      }
      const {
        email,
        nombre,
        apellido,
        password
      } = requestBody;

      const user = new User();
      user.email = email;
      user.nombre = nombre;
      user.apellido = apellido;
      user.password = password;

      const token = await generateJwt(user);
      const userRepository = getRepository(User);
      userRepository.save(user);

      return { statusCode: 200, success: true, message: 'Usuario registrado', user: user.userInfo, token } as IRegisterSuccess;
    } catch (error) {
      return { statusCode: 500, success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' } as IErrorGeneric;
    }
  }

  @Post("renew")
  public async revalidateToken(@Request() request: Req): Promise<any> {
    try {
      const { usuario } = request as any;
      const user = await getRepository(User).findOne({ where: { email: usuario.email } })

      const token = await generateJwt(user);
      return {
        success: true,
        token,
        user: usuario
      };
    } catch (error) {
      return { success: false, message: 'Ocurrio un error, contacte con el Administrador del sistema' };
    }
  }
}
