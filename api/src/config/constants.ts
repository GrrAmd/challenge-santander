require("dotenv").config();

export const APP_URL = process.env.APP_URL;
export const APP_PORT = process.env.APP_PORT;

export const SMTP_SERVICE = process.env.SMTP_SERVICE;
export const SMTP_USER = process.env.SMTP_USER;
export const SMPT_PASS = process.env.SMPT_PASS;

export const JWT_SECRET = process.env.APP_KEY;
export const APP_KEY = process.env.APP_KEY

export const VAPID_KEYS = {
  "publicKey": "BEQIaSqea3OcfR159mB0RYM4tcydOicV9s3aL5CuFkNh0Thc5GBDWuVkG0uDw1VyMxDqfljmH4CQmJTb7AVfigE",
  "privateKey": "vqfnKxcOidnAm-hr5Fhw8cWkBHvnJo8bUDZT21QShPs"
};

export const PACK_BIRRAS = 6;

export const RAPID_API = {
  HOSTNAME: "https://community-open-weather-map.p.rapidapi.com",
  X_RAPID_HOST: "community-open-weather-map.p.rapidapi.com",
  API_KEY: process.env.RAPID_API_KEY
};

/**
 * Hours to left start a meetup
 */
export const DISABLE_MEETUP = 48;

export const PROVIDER_MAIL = process.env.PROVIDER_MAIL;
