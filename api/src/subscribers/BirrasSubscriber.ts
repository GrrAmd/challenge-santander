import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from "typeorm";
import { MeetupInscription } from '../models/MeetupInscription';
import { MeetupService } from '../services/Meetup.service';

@EventSubscriber()
export class BirrasSubscriber implements EntitySubscriberInterface<MeetupInscription> {

  listenTo() {
    return MeetupInscription;
  }

}
