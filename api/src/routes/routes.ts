import { Router, Request, Response } from 'express';
import { OpenWeatherMapService } from '../services/OpenWeatherMap.service';
import { renderHbs } from '../utils/RenderHbs.util';
import auth from './auth.routes';
import meetup from './meetup.routes';
import faker from "faker";

const routes = Router();
routes.get('', (request: Request, response: Response) => {
  response.send({ info: 'SANTANDER API v1' })
});

routes.use('/meetup', meetup)
routes.use("/auth", auth);

routes.get("/test", async (req, res) => {
  try {
    const countries = [];
    for (let index = 0; index < 20; index++) {
      const city = faker.address.state()
      const country = faker.address.country();
      countries.push({city, country});
    }
    return res.json(countries);
  } catch (error) {
    console.log(error);
  }
})
export default routes;
