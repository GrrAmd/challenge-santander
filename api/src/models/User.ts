import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, BaseEntity, DeleteDateColumn, AfterInsert, BeforeInsert, Exclusion } from "typeorm";
import { compareSync, genSaltSync, hashSync } from "bcryptjs";
import { ExclusionMetadata } from 'typeorm/metadata/ExclusionMetadata';

enum Roles {
  ADMIN = 'admin',
  USER = 'user'
}
@Entity('user')
export class User extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', unique: true })
  email: string;

  @Column({ type: 'varchar' })
  nombre: string;

  @Column({ type: 'varchar' })
  apellido: string;

  @Column({ type: 'enum', enum: Roles, default: Roles.USER })
  role: Roles;

  @Column({ type: 'varchar', length: 255, select: false })
  password: string;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  createdAt: Date;

  @DeleteDateColumn({ select: false })
  deletedAt?: Date;

  hashPassword() {
    const salt = genSaltSync();
    this.password = hashSync(this.password, salt);
  }

  validatePassword(password) {
    return compareSync(password, this.password)
  }

  get fullName() {
    return `${this.apellido}, ${this.nombre}`;
  }

  get userInfo() {
    return {
      id: this.id,
      email: this.email,
      nombre: this.nombre,
      apellido: this.apellido,
      role: this.role
    }
  }

  get isAdmin() {
    return this.role == Roles.ADMIN;
  }

  get userMeetup() {
    return {
      email: this.email,
      nombre: this.nombre,
      apellido: this.apellido,
    }
  }

}
