import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, BaseEntity, DeleteDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn, Unique, Index } from "typeorm";
import { Meetup } from './Meetup';
import { User } from './User';

@Entity('meetup_user')
@Unique("UQ_INSCRIPTION", ["user", "meetup"])
export class MeetupInscription extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, user => user.id)
  @JoinColumn()
  user: User;

  @ManyToOne(() => Meetup, meetup => meetup.id, { eager: true })
  @JoinColumn()
  meetup: Meetup;

  @Column({ type: 'boolean', default: false })
  checkIn: boolean;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  createdAt: Date;

  @DeleteDateColumn({ select: false })
  deletedAt?: Date;

  @UpdateDateColumn({ select: false })
  updatedAt?: Date;
}
