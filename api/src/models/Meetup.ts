import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BaseEntity,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Unique,
  OneToOne,
} from "typeorm";
import { createSlug } from '../utils/Slug.util';
import { MeetupClima } from './MeetupClima';
import { MeetupImages } from './MeetupImages';
import { User } from './User';
@Entity('meetup')
export class Meetup extends BaseEntity {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', unique: true })
  nombre: string;

  @Column({ type: 'datetime' })
  fecha?: Date;

  @Column({ type: 'varchar' })
  duracion: string;

  @Column({ type: 'text' })
  descripcion: string;

  @Column({ type: 'varchar', nullable: true, unique: true })
  slug: string | null;

  @ManyToOne(() => User, user => user.id, { eager: true })
  @JoinColumn()
  organizador: User;

  @Column({ type: "varchar" })
  lugar: string;

  @OneToOne(() => MeetupImages, { eager: true })
  @JoinColumn()
  image: MeetupImages;

  @OneToOne(() => MeetupClima, { eager: true })
  @JoinColumn()
  clima?: MeetupClima;

  @CreateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  createdAt: Date;

  @DeleteDateColumn({ type: "timestamp", select: false })
  deletedAt?: Date;

  @UpdateDateColumn({ type: "timestamp", default: () => "CURRENT_TIMESTAMP(6)" })
  updatedAt?: Date;

  @Column({ type: 'bool', default: true })
  available: boolean

  @Column({ type: "text", default: null })
  token?: string;



  get meetupInfo() {
    return {
      id: this.id,
      nombre: this.nombre,
      fecha: this.fecha,
      duracion: this.duracion,
      descripcion: this.descripcion,
      available: this.available
    }
  }

  slugify() {
    this.slug = createSlug(this.nombre);
  }
}
