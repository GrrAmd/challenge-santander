import {BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import { Meetup } from './Meetup';

@Entity('birras_meetup')
export class BirrasMeetup extends BaseEntity {

  @PrimaryGeneratedColumn()
  id?: number;

  @OneToOne(() => Meetup)
  @JoinColumn()
  meetup: Meetup;

  @Column({type: 'int'})
  cantBirras: number;

  @Column({type: "boolean", default: false})
  mailSended: boolean;
}
