import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../config/constants';
import { User } from '../models/User';
import CryptoJS from "crypto-js";
import express, { Request } from 'express';

export async function generateJwt(usuario: User): Promise<string> {
  const payload = { id: usuario.id, usuario: usuario.userInfo };
  return new Promise((resolve, reject): void => {
    jwt.sign(payload, JWT_SECRET, {
      expiresIn: '24h'
    }, (err, token) => {
      if (err) {
        reject(err);
      } else {
        resolve(token)
      }
    });
  });
}

export function crypt(obj) {
  return CryptoJS.SHA256(JSON.stringify(obj)).toString();
}

export function decrypt(obj) {
  const dataDecoded = CryptoJS.AES.decrypt(obj, JWT_SECRET)
  return JSON.parse(dataDecoded.toString(CryptoJS.enc.Utf8).replace(/99A99/gi, "/"));
}

export function getJwt(request: Request): string {
  return request.header("authorization").split("Bearer ")[1]
}
