import * as hbs from "handlebars";
import * as fs from "fs";

export async function renderHbs(fileName, data) {
  const viewsPath = `public/${fileName}`;
  const content = fs.readFileSync(viewsPath, "utf8");
  const template = hbs.compile(content, { noEscape: true });
  return template(data);
}
