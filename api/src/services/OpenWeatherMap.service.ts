import axios, { AxiosRequestConfig } from 'axios';
import { request } from 'express';
import { RAPID_API } from '../config/constants';
import { OpenWeatherMapInterface, OpenWeatherMapResponse } from '../interfaces/OpenWeatherMap.interface';
import { KelvinToCelsius } from '../utils/TempConverter';

export class OpenWeatherMapService {
  city: string;
  private options: AxiosRequestConfig = {
    method: "GET",
    url: RAPID_API.HOSTNAME + "/forecast",
    params: { q: "" },
    headers: {
      "x-rapidapi-key": RAPID_API.API_KEY,
      "x-rapidapi-host": RAPID_API.X_RAPID_HOST,
      useQueryString: true
    }
  };

  constructor(location: string) {
    this.city = location;
    this.options.params.q = this.city;
  }

  public async getInfo(): Promise<OpenWeatherMapResponse[]> {
    return axios.request(this.options)
      .then(res => {
        const data: OpenWeatherMapInterface = res.data;
        return data.list.map((val, i) => {
          return {
            min: KelvinToCelsius(val.main.temp_min),
            max: KelvinToCelsius(val.main.temp_max),
            actual: KelvinToCelsius(val.main.temp),
            date: val.dt_txt
          };
        })
      }).catch(err => {
        return err;
      })
  }
}

