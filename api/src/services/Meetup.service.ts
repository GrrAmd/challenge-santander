import { DISABLE_MEETUP } from '../config/constants';
import { Meetup } from '../models/Meetup';
import { MeetupRepository } from '../respositories/Meetup.repository';
import { HoursDifference } from '../utils/DateDifferences.util';
import { crypt, decrypt } from '../utils/Jwt.util';
import { SendMail } from './Mailer.service';
import { renderHbs } from '../utils/RenderHbs.util';
import moment from 'moment';

export class MeetupService {
  static async updateQuantityBirras() {
    const meetups = await MeetupRepository.availableMeetups();

    meetups.forEach(async (meet) => {
      const hoursDifference = HoursDifference(meet.fecha);
      if (hoursDifference <= DISABLE_MEETUP) {
        this.informToUserCreator(meet);
        MeetupRepository.disableMeetup(meet.id);
      }
    })
  }

  static async informToUserCreator(meet: Meetup) {
    const dataEncode = crypt(meet);

    const meetup = await Meetup.findOne(meet.id, { relations: ["organizador"] })
    const { nombre, apellido, email } = meetup.organizador;
    const infoHtml = {
      ...meet,
      fecha: moment(meet.fecha).format("DD/MM/YYYY H:mm"),
      user: {
        nombre,
        apellido
      },
      link: `${process.env.APP_URL}/meetup/realizar-pedido/`,
      token: dataEncode
    }
    const html = await renderHbs("mailMeetup.hbs", infoHtml)
    await SendMail({
      to: email,
      subject: "Realiza el pedido para tu evento",
      html: html
    })
    Meetup.update({ id: meet.id }, { token: dataEncode });
  }
}
