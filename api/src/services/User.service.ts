import { Request } from 'express';
import * as jwt from "jsonwebtoken";
import { JWT_SECRET } from '../config/constants';
import { MeetupInscriptionRepository } from '../respositories/MeetupInscription.repository';
import { UserRepository } from '../respositories/User.repository';
import { getJwt } from '../utils/Jwt.util';

export class UserService {
  public async getUserJwt(request: Request) {
    try {
      const token = getJwt(request);

      const { usuario } = jwt.verify(token, JWT_SECRET) as any;
      return UserRepository.getUser(usuario.id);
    } catch (error) {
      throw new Error(error);
    }
  }

  static async getUserJwt(request: Request) {
    try {
      const token = request.header('x-api-key');

      const { usuario } = jwt.verify(token, JWT_SECRET) as any;
      return UserRepository.getUser(usuario.id);
    } catch (error) {
      throw new Error(error);
    }
  }

  public async subscribeToMeetup(request: Request) {
    try {
      const user = await this.getUserJwt(request);
      const { meetup } = request.params;
      return MeetupInscriptionRepository.create(user, Number(meetup));
    } catch (error) {
      throw new Error(error);
    }
  }
}
