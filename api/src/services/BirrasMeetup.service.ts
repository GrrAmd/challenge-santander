import moment from 'moment';
import { writeFileSync } from 'fs';
import { PROVIDER_MAIL } from '../config/constants';
import { OpenWeatherMapResponse } from '../interfaces/OpenWeatherMap.interface';
import { User } from '../models/User';
import { BirrasMeetupRepository } from '../respositories/BirrasMeetup.repository';
import { MeetupRepository } from '../respositories/Meetup.repository';
import { MeetupInscriptionRepository } from '../respositories/MeetupInscription.repository';
import { UserRepository } from '../respositories/User.repository';
import { ApproachDate, HoursDifference } from '../utils/DateDifferences.util';
import { renderHbs } from '../utils/RenderHbs.util';
import { SendMail } from './Mailer.service';
import { OpenWeatherMapService } from './OpenWeatherMap.service';
import { Meetup } from '../models/Meetup';
import { MeetupClima } from '../models/MeetupClima';
import { normalizeStr } from '../utils/NormalizeText';

export class BirrasMeetupService extends MeetupRepository {

  public async makeOrder(token: string) {
    try {
      const meet = await this.findByToken(token);
      const { email } = meet.organizador;
      const members = await MeetupInscriptionRepository.countMembers(meet.id);

      const weatherInfo = await new OpenWeatherMapService(normalizeStr(meet.lugar)).getInfo();
      const temps = this.getApproachInfo(meet.fecha, weatherInfo);

      const cantBirras = this.calculateOrder(members, temps);
      const horaEntrega = moment(meet.fecha).format("DD/MM/YYYY HH:mm");

      const html = await renderHbs("mailPedido.hbs", {
        meet, cantBirras, horaEntrega
      });
      SendMail({
        from: email,
        to: PROVIDER_MAIL,
        subject: "Pedido de cervezas Meetup",
        html: html
      })
      BirrasMeetupService.changeToken(meet);
      return "Ya le enviamos el mail a tu proveedor";
    } catch (error) { 
      throw new Error("No se pudo enviar el mail");
    }
  }

  public async setClimaMeetup() {
    try {
      const meetups = await Meetup.find();

      for (const meetup of meetups) {
        if (!meetup.clima) {
          const hours = HoursDifference(meetup.fecha);
          if (hours < 72) {
            const weatherInfo = await new OpenWeatherMapService(normalizeStr(meetup.lugar)).getInfo();
            const temps = this.getApproachInfo(meetup.fecha, weatherInfo);
            const climaMeetup = new MeetupClima()
            climaMeetup.date = meetup.fecha;
            climaMeetup.tempAprox = temps;
            climaMeetup.rain = false;
            await climaMeetup.save()

            meetup.clima = climaMeetup;
            await meetup.save();
          }
        }
      }
    } catch (error) {
      console.log("No se pudo asignar el clima", error)
      throw new Error("No se pudo asignar el clima");

    }
  }

  private calculateOrder(members: number, temp) {
    let base = 0;
    switch (true) {
      case (temp >= 20 && temp <= 24):
        base = 1
        break;
      case (temp < 20):
        base = 0.75
        break;
      case (temp > 24):
        base = 3.75
        break;
    }

    const cantCajas = ((base * members) / 6) * 1.20;
    return Number(cantCajas).toFixed(0);
  }

  private getApproachInfo(meetupDate, weatherInfo: OpenWeatherMapResponse[]) {
    try {
      writeFileSync("log.json", JSON.stringify({ meetupDate, weatherInfo }))
      const dateToFind = ApproachDate(meetupDate, weatherInfo, "date");
      const climaIndex = weatherInfo.findIndex(x => x.date == dateToFind);
      return weatherInfo[climaIndex].actual;
    } catch (error) {
      throw new Error("No se pudo encontrar una fecha aproximada");
    }
  }
}
